const { user_game, user_game_biodata } = require("../models");

class registerController {
  static renderRegister(req, res) {
    res.status(200).render("register");
  }

  static registerCreate(req, res) {
    const data = req.body;
    user_game
      .create({
        username: data.username,
        email: data.email,
        password: data.password,
        isAdmin: data.isAdmin,
      })
      .then((result) => {
        user_game_biodata.create({
          username: data.username,
          email: data.email,
          fullname: data.fullname,
          birthday: data.birthday,
          address: data.address,
          contact: data.contact,
          user_id: data.user_id,
        });
      })
      .then(() => {
        res.status(201).redirect("/login");
      })
      .catch((error) => {
        res.status(500).json({ message: "There is something error in your request" });
      });
  }
}

module.exports = { registerController };
