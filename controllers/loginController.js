const { user_game } = require("../models");
class loginController {
  static renderLogin(req, res) {
    res.status(200).render("login");
  }
  static getLogin(req, res) {
    const data = req.body;
    user_game
      .findOne({
        where: {
          username: data.username,
          password: data.password,
        },
      })
      .then((result) => {
        if (!result) {
          res.redirect("/login");
        } else {
          res.redirect("/game");
        }
      });
  }
}

module.exports = { loginController };
