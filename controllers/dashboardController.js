const { user_game, user_game_biodata } = require("../models");
class dashboardController {
  static renderDashboard(req, res) {
    user_game_biodata.findAll().then((data) => {
      res.render("dashboard", { users: data });
    });
  }

  static pageUpdateBio(req, res) {
    if (req.query.error) {
      if (req.query.error == "invalid input") {
        errors.push("username & email wajib diisi");
      }
    }
    res.send(errors);
    user_game
      .findOne({
        where: {
          id: req.params.id,
        },
      })
      .then((data) => {
        res.render("update", { user: data });
      });
  }
  static updateBiodata(req, res) {
    const data = req.body;
    if (email == "" || email == null) {
      req.session.error = "email harus diisi";
      res.redirect(`/user/${req.params.id}/update?error=invalid-input`);
    }
    user_game
      .update(
        {
          // Value will be update
          username: data.username,
          email: data.email,
          fullname: data.fullname,
          birthday: data.birthday,
          address: data.address,
          contact: data.contact,
        },
        {
          // Scope value want to update
          where: {
            id: req.params.id,
          },
        }
      )
      .then((result) => {
        res.redirect("/dasboard");
      })
      .catch((err) => {
        res.status(500).json(err);
      });
  }

  static pageDeleteBio(res, req) {
    user_game
      .destroy({
        where: {
          id: req.params.id,
        },
      })
      .then((data) => {
        res.redirect("/dashboard");
      })
      .catch((err) => {
        res.status(500).json(err);
      });
  }

  static pageShowBio(res, req) {
    user_game
      .findOne({
        where: {
          id: req.params.id,
        },
      })
      .then((data) => {
        res.render("show", { user: data });
      })
      .catch((err) => {
        res.status(500).json(err);
      });
  }
}

module.exports = { dashboardController };
