"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class user_game_biodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_game_biodata.belongsTo(models.user_game);
    }
  }
  user_game_biodata.init(
    {
      username: DataTypes.STRING,
      email: DataTypes.STRING,
      fullname: DataTypes.STRING,
      birthday: DataTypes.DATEONLY,
      address: DataTypes.STRING,
      contact: DataTypes.STRING,
      date_register: DataTypes.DATE,
      user_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "user_game_biodata",
    }
  );
  return user_game_biodata;
};
