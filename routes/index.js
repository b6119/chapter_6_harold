const express = require("express");
const cors = require("cors");
const router = express.Router();

const { home } = require("../controllers/homeController");
const { loginController } = require("../controllers/loginController");
const { registerController } = require("../controllers/registerController");
const { gameController } = require("../controllers/gameController");
const { dashboardController } = require("../controllers/dashboardController");

router.route("/", cors()).get(home);
router.route("/login", cors()).get(loginController.renderLogin).post(loginController.getLogin);
router.route("/register", cors()).get(registerController.renderRegister).post(registerController.registerCreate);
router.route("/game", cors()).get(gameController.renderGame);
router.route("/dashboard", cors()).get(dashboardController.renderDashboard);
router.route("/user/:id/update", cors()).get(dashboardController.pageUpdateBio).post(dashboardController.updateBiodata);
router.route("/user/:id/delete", cors()).get(dashboardController.pageDeleteBio);
router.route("/user/:id/details", cors()).get(dashboardController.pageShowBio);
module.exports = router;
