// Module
const express = require("express");
const path = require("path");
const session = require("express-session");
const router = require("./routes/index");
// Listen Port
const PORT = 3000;

// Initialization
const app = express();

// Parser
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Session
app.use(
  session({
    secret: "Harold",
    isLogin: false,
    isAdmin: false,
    username: "",
  })
);

// Static Files
app.use(express.static(path.join(__dirname, "public")));

// View Engine
app.set("view engine", "ejs");

// Load all application
app.use(router);

// Listen the server at avaiable PORT
app.listen(PORT, () => {
  console.log(`Server berjalan pada http://localhost:${PORT}`);
});
